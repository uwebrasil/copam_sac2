


# SAC - Sistema Agenda de Contatos
 
## Description

O sistema deverá auxiliar no registro dos contatos telefônicos, email e
endereço dos clientes afim de manter de maneira centralizada os contatos de empresa.
This will be a log of installation and development.

## Getting Started

### Environment
* Windows 7, 32bit, Firefox 78.0.1 (32bit)
* XAMPP Webdevelopment Stack
* Visual Studio Code / XDebug
 ```
  C:\xampp\htdocs\copam\sac>php --version
 ```
 *PHP 7.2.5 (cli) (built: Apr 25 2018 02:39:21) ( ZTS MSVC15 (Visual C++ 2017) x86 )
  Copyright (c) 1997-2018 The PHP Group
  Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies*

    C:\xampp\htdocs\copam\sac>php artisan -V
   *Laravel Framework 7.26.1*

  ```
  console.log(JQuery().jquery)
  ``` 
  *3.5.1
### Dependencies 
> PHP 7.2.5
> Laravel 7.26.1
> JQuery 3.5.1
> Bootstrap 4.5.2

### Readings
* https://laravel.com/docs/7.x
* https://www.codeandtuts.com/laravel-6-crud-tutorial/
* https://www.techiediaries.com/laravel/laravel-6-7-tutorial-new-features-mysql-database/
*  much more ...
### Setup and Configuration (not complete)

    laravel new sac

>mockery/mockery 1.4.2 requires php ^7.3 || ^8.0 -> your PHP version (7.2.5) does not satisfy that requirement.
**Solution:** 
https://stackoverflow.com/questions/61900738/laravel-new-blog-not-work-mockery-mockery-1-4-0
delete composer.lock and run composer update

    C:\xampp\htdocs\copam\sac>php artisan key:generate

>**Application key** set successfully.
  

      ren .env.example .env

> configure database credentials 
   add application key

    phpMyAdmin
> create database, set charset to utf8mb_general_ci
>create login/register tables

    php artisan migrate  (create login/register)
>Illuminate\Database\QueryException
SQLSTATE[42000]: Syntax error or access violation: 1071 Specified key was too long;
**Solution:**
https://laravel-news.com/laravel-5-4-key-too-long-error
modify boot-method in AppServiceProvider.php

**authentication scaffold Login/Register**

    composer require laravel/ui --dev
    php artisan ui bootstrap --auth 
    npm install && npm run dev

**localization**
veja: https://github.com/lucascudo/laravel-pt-BR-localization

**how to seed**
-edit database/DatabaseSeeder.php

    composer dump-autoload
    php artisan db:seed

**bootstrap4** modal delete confirmation
- https://gist.github.com/milon/030f12aaaf54dd178657

**custom Validation CPF/CNPJ and CEP**
- app/Providers/AppServiceProvider.php
https://www.guj.com.br/t/laravel-validar-cpf/382494/2
https://www.geradorcnpj.com/script-validar-cnpj-php.htm

### Validação CEP via webservice VIACEP 
* Validação clientside  via Javascript/Json (todo: autopreencher campos se cep válido)
-https://viacep.com.br/exemplo/javascript/
* Validação serverside via PHP/XML
-http://viacep.com.br/ws/$cep/xml/
-https://zerobugs.com.br/ver-post/como-pegar-endereco-completo-atraves-do-cep-usando-php-45/
 
 ### Selectbox  autocomplete via Ajax
 * 10k clientes demora 4seg para carregar
-melhorar utilizabilidade via autocomplete
-https://phppot.com/jquery/jquery-ajax-autocomplete-country-example/
 
### Problemas e obstáculos
* Laravel adds plural 's' to database tables by convention
   to avoid: Model -> protected $table = 'singular';
* Laravel assumes
   - primaryKey field with name id
   - primaryKey field is autoincrement
   
   use of custom primary key name (protected $primaryKey = 'id_cidade';)
   does not work, so we can't use functions which rely on this like *find,* *findOrFail* etc.
 so using DB methods (use *Illuminate\Support\Facades\DB;*)

### TODO
* configure SWIFT Mailer for 'password forgotten'

* Selectboxes->edit manter o valor velho in errorcase		DONE

* Validation unique fields ??

* Pagination count pages

* App - Configuration
-o sistema vai/ vai nao verificar CPF/CNPJ
-...
* custum validation error messages
* carregar cidades de VIACEP
* create enderco - preselect in errorcase
* not necessary send clientes-array if filled with ajax
* buscar clientes, erro na paginacao, redirecionando para methodo show??
   usar searchController ??  

### Observacoes
* como cadastrar cliente com mais de um email só ?
   ( campo email ode ser campo da tabela contatos)
 * tabela bairro e pai da tabela cidade ??  
 * campo cliente unique ??