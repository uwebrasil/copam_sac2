<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnderecosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enderecos', function (Blueprint $table) {
            $table->increments('id_endereco');           

            $table->integer('bairro_id_bairro')->unsigned();
            $table->foreign('bairro_id_bairro','endereco_FKIndex2')->references('id_bairro')->on('bairros')->onDelete('cascade');

            $table->integer('cliente_id_cliente')->unsigned();
            $table->foreign('cliente_id_cliente','endereco_FKIndex1')->references('id_cliente')->on('clientes')->onDelete('cascade');
            
            $table->string('cep')->nullable();
            $table->string('logradouro')->nullable();
            $table->string('numero')->nullable();
            $table->string('complemento')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enderecos');
    }
}
