<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBairrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bairros', function (Blueprint $table) {
            $table->increments('id_bairro');           
            $table->integer('cidade_id_cidade')->unsigned();
            $table->foreign('cidade_id_cidade','bairro_FKIndex1')->references('id_cidade')->on('cidades')->onDelete('cascade');
            $table->string('nome')->nullable();     
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bairros');
    }
}
