<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->increments('id_contato');           
            $table->integer('cliente_id_cliente')->unsigned();
            $table->foreign('cliente_id_cliente','contato_FKIndex1')->references('id_cliente')->on('clientes')->onDelete('cascade');
            $table->string('ddd')->nullable();     
            $table->string('telefone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contatos');
    }
}
