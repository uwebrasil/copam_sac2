<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        for ($i=0; $i < 10000; $i++) { 
            DB::table('clientes')->insert([
                'nome' => Str::random(20),
                'tipo_pessoa' => '',
                'cpf_cnpj' => '',
                'email' => Str::random(10).'@gmail.com',         
            ]); 
        }       
    }
}
