

@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
   <span>Endereços&nbsp;editar</span>
</div>
@endsection
@section('content')
<style>
   .uper {
   margin-top: 40px;
   }
</style>
<div class="card uper">
   <div class="card-header">
      Editar Endereço
   </div>
   <div class="card-body">
      @if ($errors->any())
      <div class="alert alert-danger">
         <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
         </ul>
      </div>
      <br />
      @endif
      <form method="post" action="{{ route('enderecos.update', $endereco->id_endereco ) }}">
         <label for="cliente_id_cliente">Cliente</label>
         <div class="input-group mb-3">
            @csrf
            @method('PATCH')
            <select class="custom-select" id="cliente_id_cliente" name="cliente_id_cliente">
               <option selected>Escolher cliente...</option>
               @foreach ($clientes as $cliente)
               <option  @if($selectedCliente == $cliente->id_cliente) selected @endif
               value="{{ $cliente->id_cliente }}">{{ $cliente->nome }}</option>
               @endforeach
            </select>
         </div>
         <label for="bairro_id_bairro">Bairro</label>
         <div class="input-group mb-3">
            <select class="custom-select" id="bairro_id_bairro" name="bairro_id_bairro">
               <option selected>Escolher bairro...</option>
               @foreach ($bairros as $bairro)
               <option  @if($selectedBairro == $bairro->id_bairro) selected @endif
               value="{{ $bairro->id_bairro }}">{{ $bairro->nome }}</option>
               @endforeach
            </select>
         </div>
         <div class="form-group">              
            <label for="cep">CEP: Favor, informar só digitos!</label>
            <input type="text" class="form-control" name="cep" id="cep"  
               value="{{ $endereco->cep }}"
               onkeypress="return isNumberKey(event)"                   
               onblur="pesquisacep(this.value);"
               maxlength="8"
               />
         </div>
         <div class="form-group">              
            <label for="logradouro">Logradouro:</label>
            <input type="text" class="form-control" name="logradouro" id="logradouro"  
               value="{{ $endereco->logradouro }}"
               />
         </div>
         <div class="form-group">              
            <label for="numero">Número:</label>
            <input type="text" class="form-control" name="numero" id="numero"  
               value="{{ $endereco->numero }}"
               onkeypress="return isNumberKey(event)"/>
         </div>
         <div class="form-group">              
            <label for="complemento">Complemento:</label>
            <input type="text" class="form-control" name="complemento" id="complemento"  
               value="{{ $endereco->complemento }}"
               />
         </div>
         <button type="submit" class="btn btn-primary">Alterar</button>
      </form>
   </div>
</div>
@endsection

