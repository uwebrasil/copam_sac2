

@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
   <span>Endereços&nbsp;cadastrar</span>
</div>
@endsection
@section('content')
<style>
   .uper {
   margin-top: 40px;
   }
</style>
<div class="card uper">
   <div class="card-header">
      SAC - adicionar endereço
   </div>
   <div class="card-body">
      <div id="jqueryError" class="alert alert-danger" style="display:none">
      </div>
      @if ($errors->any())         
      <div class="alert alert-danger">
         <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
         </ul>
      </div>
      <br />
      @endif
      <!-- // determinar de onde o arquivo foi chamado ( Novo Cliente (A) | listar->adicionar (B) ) -->   
      <!--
         first call A -> set hidden field for id_cliente from previous url
         errorcase A (second call) -> check if hidden field exists
         if yes, update $fromNovoCliente and $cliente_id  for preselect selectbox   
         
         save old input in error case
         -->
      @php
       //var_dump(session()->all());
       //var_dump($clientes);
      $cep = "";
      $logradouro ="";
      $numero = "";
      $complemento = "";     
      $bairro_id = "";

      $url = session()->all()['_previous']['url'] ;
      $fromNovoCliente = strpos( $url,'clientes') != false ; 
      $cliente_id = Str::after($url,'clientes/') ;
      //var_dump('A');
      //var_dump($cliente_id);
      if($errors->any()) {      
       $isHidden =  isset( session()->all()['_old_input']['hidden'] );
      if ($isHidden) {
        $fromNovoCliente= true;
      }   
      
      $cliente_id = session()->all()['_old_input']['cliente_id_cliente'] ;  
      $cep = session()->all()['_old_input']['cep'] ;  
      $logradouro =session()->all()['_old_input']['logradouro'] ;  
      $numero =session()->all()['_old_input']['numero'] ;  
      $complemento = session()->all()['_old_input']['complemento'] ;  
      $bairro_id = session()->all()['_old_input']['bairro_id_bairro'] ;     
      }
     
      @endphp 
      
      <form  method="post" action="{{ route('enderecos.store') }}">
         <label for="bairro_id_bairro">Bairro</label>          
         <div class="input-group mb-3">
            @csrf
            <!-- pass id do novo cliente to endereco.store -->
            @if($fromNovoCliente)
            <input name="hidden" type="hidden" value="{{ $cliente_id }}"/> 
            @endif

            <select class="custom-select" id="bairro_id_bairro" name="bairro_id_bairro">
               <option selected>Escolher bairro...</option>
               @foreach ($bairros as $bairro)                  
                 <option               
                   @if(($bairro->id_bairro == $bairro_id)) selected  @endif 
                   value="{{ $bairro->id_bairro }}">{{ $bairro->nome }}</option>               
               @endforeach
            </select>
         </div>


         <label for="cliente_id_cliente">Cliente</label>  
         <div  class="form-group">
             <!--<label for="search">Buscar:</label>-->
	          <input type="text" autocomplete="off" 
             class="form-control" id="search-box" name="search-box" 
             value="{{session()->all()['_old_input']['search-box'] ?? ''}}"
             placeholder="procurar cliente a escolher"/>
         </div>  
         <div class="input-group mb-3">
            <select class="custom-select myauto" id="cliente_id_cliente" name="cliente_id_cliente">
               <option selected>Escolher cliente...</option>
               @if ($errors->any() OR ($fromNovoCliente == true))  
                     @foreach ($clientes as $cliente)       
                       @if( ($cliente->id_cliente == $cliente_id) )                         
                         <option 
                            selected 
                            value="{{ $cliente->id_cliente }}">{{ $cliente->nome }}
                         </option>   
                       @endif            
                     @endforeach  
               @endif
            </select>
         </div>
                          
         <div class="form-group">              
            <label for="cep">CEP:  Favor, informar só digitos(8)!</label>
            <input type="text" class="form-control" name="cep" id="cep" 
               value="{{$cep}}"
               maxlength="8"
               onblur="pesquisacep(this.value);"
               onkeypress="return isNumberKey(event)"/>
         </div>
         <div class="form-group">              
            <label for="logradouro">Logradouro:</label>
            <input type="text" class="form-control" name="logradouro" id="logradouro" value="{{$logradouro}}"/>
         </div>
         <div class="form-group">              
            <label for="numero">Número:</label>
            <input type="text" class="form-control" name="numero" id="numero" value="{{$numero}}" onkeypress="return isNumberKey(event)" />
         </div>
         <div class="form-group">              
            <label for="complemento">Complemento:</label>
            <input type="text" class="form-control" name="complemento" id="complemento" value="{{$complemento}}"/>
         </div>
         <button type="submit" class="btn btn-primary">Adicionar</button>
      </form>
   </div>
</div>
@endsection

