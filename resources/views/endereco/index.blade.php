

@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
   <span>Endereços&nbsp;listar</span>
</div>
@endsection
@section('content')
<style>
   .uper {
   margin-top: 40px;
   }
</style>
<div class="uper">
@if(session()->get('success'))
<div class="alert alert-success">
   {{ session()->get('success') }}  
</div>
<br />
@endif
@include('components.modal')
{{ $enderecos->links() }}
<table class="table table-striped" data-toggle="dataTable" data-form="deleteForm">
   <thead>
      <tr>
         <td>Cliente</td>
         <td>Bairro</td>
         <td>CEP</td>
         <td>Logradouro</td>
         <td>Número</td>
         <td>Complemento</td>
         <td>&nbsp;</td>
         <td><a href="{{ route('enderecos.create')}}" class="btn btn-outline-success btn-sm">
            <i class="fa fa-plus"></i>&nbsp;Adicionar</i></a>
         </td>
      </tr>
   </thead>
   <tbody>
      @foreach($enderecos as $endereco)       
      <tr>
         <td>{{$endereco->cnome}}</td>
         <td>{{$endereco->bnome}} ({{$endereco->city}})</td>
         <td>{{ Helper::formatCEP($endereco->cep)}} </td>
         <td>{{$endereco->logradouro}}</td>
         <td>{{$endereco->numero}}</td>
         <td>{{$endereco->complemento}}</td>
         <td><a href="{{ route('enderecos.edit', $endereco->id_endereco)}}" class="btn btn-outline-primary btn-sm">
            <i class="fa fa-edit"></i>&nbsp;Editar</i></a>
         </td>
         <td>         
         <form method="POST" action="{{ route('enderecos.destroy', $endereco->id_endereco) }}" class="form-inline form-delete">
            @csrf
            @method('DELETE')
            <hidden name="id" value="{{$endereco->id_endereco}}">
            <button type="submit" class="btn btn-outline-danger btn-sm" name="delete_modal" 
               data-toggle="tooltip" title='Delete'><i class="fa fa-trash"></i>&nbsp;Deletar</button>
         </form>
         </td>
      </tr>
      @endforeach       
   </tbody>
</table>
<div>
@endsection

