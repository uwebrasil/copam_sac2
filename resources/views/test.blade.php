
<!DOCTYPE html>
<html>
<head>
    <title>Ajax Request Test </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}"/>
   
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
      <script src="{{ asset('js/app.js') }}" type="text/js"></script>

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>        

      <style>
       .item {
           background-color: "#0000ff";
           color: "white";
       }
      </style>
    
</head>
<body>
  
    <div class="container">
        <h1>Ajax Request Test</h1>
       
    <div id ="uwe"></div>
    <br />
    <button class="btn btn-success btn-info" id="ajaxSubmit" >AjaxSubmit GET</button>
    <br />
    <br />
    <button class="btn btn-success btn-info" id="ajaxSubmitPost">AjaxSubmit POST</button>
    <br />
    <div id="result">
     <ul id="liste"></ul>
    </div>
   

    <div  class="form-group">
       <label for="search">Buscar:</label>
	   <input type="text" class="form-control" id="search-box" name="search-box" placeholder="nome cliente"/>
    </div>   

    suggestion   
	<div id="suggesstion-box">UWE</div>

    <hr>
    <div class="form-group">              
            <label for="xxx">TestBox</label>
            <input type="text" class="form-control" name="xxx" id="xxx" 
               value=""              
            />
    </div> 

    <label for="cliente_id_cliente">Cliente</label>   
 
    <div class="input-group mb-3">
        <select class="custom-select">
        <option></option>
        <option value="PA">Pennsylvania</option>
        <option value="CT">Connecticut</option>
        <option value="NY">New York</option>
        <option value="MD">Maryland</option>
        <option value="VA">Virginia</option>
        </select>
    </div>

    </div>

   <!--  <label for="cliente_id_cliente">Cliente</label>  
         <div class="input-group mb-3">
            <select class="custom-select" id="cliente_id_cliente" name="cliente_id_cliente">
               <option selected>Escolher cliente...</option>
               
            </select>
         </div> -->



</div>
      
</body>

<script>

    $(document).ready(function()
    {
       $("#uwe").html("Yasmin");

       jQuery(document).ready(function(){

            jQuery('#ajaxSubmit').click(function(e){
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')                      
                  }
               });
               jQuery.ajax({                   
                  url: "{{  route('getClientes') }}",
                  method: 'get',
                  data: {},
                  success: function(result){
                    
                     console.log(result);
                     $.each(result, function(cnt, v) {
                        
                        //console.log('LOOP');
                        //console.log(k);
                        console.log(v);
                        //debugger;
                        var li = $("<li></li>").text(cnt + "=> " + v['id_cliente'] + " : " + v['nome']);
                        $("#liste").append(li);

                     });                     

                  },
                  error: function (xhr, ajaxOptions, thrownError) {
                     alert(xhr.status);
                     alert(thrownError);
                  }         
                });
               });
            });

            ///POST

            $("#search-box").keyup(function(){
               
                $.ajax({
                type: "post",
                url: "{{ route('postClientes') }}",
            
                data: {"_token": "{{ csrf_token() }}","id": $(this).val()},

                beforeSend: function(){                   
                    $("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");

                },
                success: function(data){
                   
                    $("#suggesstion-box").html("");
                    $("#suggesstion-box").show();
                    //$("#suggesstion-box").html(data);
                    $("#search-box").css("background","#FFF");
                    $('.custom-select').empty();
                    $('.custom-select').append("<option value="+"-1"+">"+"Escolher cliente"+"</option>");
                    $.each(data, function(index, value) {                      
                      //$("#suggesstion-box").append("<br />"+value.nome);
                      $("#suggesstion-box").append("<div style=\"color:white;background-color:blue\" class=\"item\">"+value.nome) + "</div>";
                     
                      $('.custom-select').append("<option value="+value.id_cliente+">"+value.nome+"</option>");
                    }); 
                },
                error: function (xhr, ajaxOptions, thrownError) {
                     alert(xhr.status);
                     alert(thrownError);
                     debugger;
                  }   
                }); 
            });
            //To select cliente
             function selectCliente(val) {
                $("#search-box").val(val);
                $("#suggesstion-box").hide();
            }  

            });   
   
</script>
   
</html> 