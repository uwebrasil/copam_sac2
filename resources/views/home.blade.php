@extends('layouts.app')

@section('content')
<div class="container">  

  <div class="card-columns">
      <!-- CLIENTES -->
      <div class="card bg-primary">
          <div class="card-body text-center">
              <a href="{{ url('/') }}/clientes"><p class="card-text" style="color:white">Clientes</p></a>  
          </div>
      </div>
     
       <!-- CIDADES -->
       <div class="card bg-primary">
          <div class="card-body text-center">
              <a href="{{ url('/') }}/cidades"><p class="card-text" style="color:white">Cidades</p></a>  
          </div>
      </div>
       <!-- NOMEENDERECO -->
       <div class="card bg-primary">
          <div class="card-body text-center">
              <a href="{{ url('/') }}/clientes/nomeEndereco"><p class="card-text" style="color:white">Listagem Nome/Endereço</p></a>              
          </div>
      </div>
      <!-- CONTATOS -->
      <div class="card bg-primary">
          <div class="card-body text-center">
              <a href="{{ url('/') }}/contatos"><p class="card-text" style="color:white">Contatos</p></a>  
          </div>
      </div>
      
      
       <!--DUMMY -->
       <div class="card bg-success">
          <div class="card-body text-center">
          <a href="{{ url('/') }}/clientes/create"><p class="card-text" style="color:white;">Novo Cliente</p></a>                          
          </div>
      </div>
       <!-- NOMETELEFONE -->
       <div class="card bg-primary">
          <div class="card-body text-center">
              <a href="{{ url('/') }}/clientes/nomeTelefone"><p class="card-text" style="color:white">Listagem Nome/Telefone</p></a>              
          </div>
      </div>
      <!-- ENDEREÇOS -->
      <div class="card bg-primary">
          <div class="card-body text-center">
              <a href="{{ url('/') }}/enderecos"><p class="card-text" style="color:white">Endereços</p></a>  
          </div>
      </div>

       <!-- BAIRROS -->
       <div class="card bg-primary">
          <div class="card-body text-center">
              <a href="{{ url('/') }}/bairros"><p class="card-text" style="color:white">Bairros</p></a>  
          </div>
      </div>

      <!-- DUMMY -->
      <div class="card bg-primary">
          <div class="card-body text-center">
          <a href="{{ url('/') }}/clientes/buscarNomeForm"><p class="card-text" style="color:white">Buscar por nome</p></a>             
          </div>
      </div>
       <!--DUMMY -->
       <!--
       <div class="card bg-primary">
          <div class="card-body text-center">
          <a href="{{ url('/') }}/clientes/checkCEP"><p class="card-text" style="color:white">Check CEP</p></a>                          
          </div>
      </div>
      -->

       
      
   </div>

</div>
@endsection
