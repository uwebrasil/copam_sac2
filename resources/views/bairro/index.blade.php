@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
   <span>Bairros&nbsp;listar</span>
</div>
@endsection
@section('content')
<style>
   .uper {
   margin-top: 40px;
   }
</style>
<div class="uper">
@if(session()->get('success'))
<div class="alert alert-success">
   {{ session()->get('success') }}  
</div>
<br />
@endif
@include('components.modal')
{{ $bairros->links() }}
<table class="table table-striped" data-toggle="dataTable" data-form="deleteForm">
   <thead>
      <tr>
         <td>Bairro</td>
         <td>Cidade</td>
         <td>&nbsp;</td>
         <td><a href="{{ route('bairros.create')}}" class="btn btn-outline-success btn-sm"><i class="fa fa-plus"></i>&nbsp;Adicionar</a></td>
      </tr>
   </thead>
   <tbody>
      @foreach($bairros as $bairro)       
      <tr>
         <td>{{$bairro->nome}}</td>
         <td>{{$bairro->cnome}}</td>
         <td><a href="{{ route('bairros.edit', $bairro->id_bairro)}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-edit"></i>&nbsp;Editar</a></td>
         <td>
            <form action="{{ route('bairros.destroy', $bairro->id_bairro)}}" method="post" class="form-inline form-delete">
               @csrf
               @method('DELETE')
               <hidden name="id" value="{{$bairro->id_bairro}}">
               <button type="submit" class="btn btn-outline-danger btn-sm" name="delete_modal" 
                  data-toggle="tooltip" title='Delete'><i class="fa fa-trash"></i>&nbsp;Deletar</button>
            </form>
         </td>
      </tr>
      @endforeach       
   </tbody>
</table>
<div>
@endsection