@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
   <span>Bairros&nbsp;editar</span>
</div>
@endsection
@section('content')
<style>
   .uper {
   margin-top: 40px;
   }
</style>
<div class="card uper">
   <div class="card-header">
      Editar Bairro
   </div>
   <div class="card-body">
      @if ($errors->any())
      <div class="alert alert-danger">
         <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
         </ul>
      </div>
      <br />
      @endif
      <form method="post" action="{{ route('bairros.update', $bairro->id_bairro ) }}">
         <div class="form-group">
            @csrf
            @method('PATCH')
            <label for="nome">Nome:</label>
            <input type="text" class="form-control" name="nome" value="{{ $bairro->nome }}"/>
         </div>
         <div class="input-group mb-3">
            <select class="custom-select" id="cidade_id_cidade" name="cidade_id_cidade">
               <option selected>Escolher cidade...</option>
               @foreach ($cidades as $cidade)
               <option 
               @if($selectedCidade == $cidade->id_cidade) selected @endif
               value="{{ $cidade->id_cidade }}">{{ $cidade->nome }}</option>
               @endforeach
            </select>
         </div>
         <button type="submit" class="btn btn-primary">Alterar</button>
      </form>
   </div>
</div>
@endsection