@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
    <span>Cidades&nbsp;editar</span>
</div>
@endsection
@section('content')
<style>
    .uper {
    margin-top: 40px;
    }
</style>
<div class="card uper">
    <div class="card-header">
        Editar Cidade
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('cidades.update', $cidade->id_cidade ) }}">
            <div class="form-group">
                @csrf
                @method('PATCH')
                <label for="nome">Nome:</label>
                <input type="text" class="form-control" name="nome" value="{{ $cidade->nome }}"/>
            </div>
            <div class="input-group mb-3">
                <select class="custom-select" id="UF" name="UF">
                    <option selected>Escolher...</option>
                    @foreach ($ufs as $uf)                
                    <option  @if($selectedUF == $uf->UF) selected @endif
                    value="{{ $uf->UF }}">{{ $uf->Estado }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Alterar</button>
        </form>
    </div>
</div>
@endsection