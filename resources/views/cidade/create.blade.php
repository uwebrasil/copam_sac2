@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
    <span>Cidades&nbsp;cadastrar</span>
</div>
@endsection
@section('content')
<style>
    .uper {
    margin-top: 40px;
    }
</style>
<div class="card uper">
    <div class="card-header">
        SAC - adicionar cidade
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form name="storeCity" method="post" action="{{ route('cidades.store') }}">
            <div class="form-group">
                @csrf
                <label for="nome">Nome:</label>
                <input type="text" class="form-control" name="nome"/>
            </div>
            <div class="input-group mb-3">
                <select class="custom-select" id="UF" name="UF">
                    <option selected>Escolher...</option>
                    @foreach ($ufs as $uf)
                    <option value="{{ $uf->UF }}">{{ $uf->Estado }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" id="btn-submit" class="btn btn-primary">Adicionar</button>
        </form>
    </div>
</div>
@endsection