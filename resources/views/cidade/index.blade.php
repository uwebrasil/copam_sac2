@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
    <span>Cidades&nbsp;listar</span>
</div>
@endsection
@section('content')
<style>
    .uper {
    margin-top: 40px;
    }
</style>
<div class="uper">
@if(session()->get('success'))
<div class="alert alert-success">
    {{ session()->get('success') }}  
</div>
<br />
@endif
@include('components.modal')
{{ $cidades->links() }}
<table class="table table-striped" data-toggle="dataTable" data-form="deleteForm">
    <thead>
        <tr>
            <!-- <td>ID</td> -->
            <td>Cidade</td>
            <td>UF</td>
            <td>&nbsp;</td>
            <td><a href="{{ route('cidades.create')}}" class="btn btn-outline-success btn-sm"><i class="fa fa-plus"></i>&nbsp;Adicionar</a></td>
        </tr>
    </thead>
    <tbody>
        @foreach($cidades as $cidade)       
        <tr>
            <!-- <td>{{$cidade->id_cidade}}</td> -->
            <td>{{$cidade->nome}}</td>
            <td>{{$cidade->UF}}</td>
            <td><a href="{{ route('cidades.edit', $cidade->id_cidade)}}" class="btn btn-outline-primary btn-sm"> <i class="fa fa-edit"></i>&nbsp;Editar</a></td>
            <td>
                <!--  <form action="{{ route('cidades.destroy', $cidade->id_cidade)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-outline-danger btn-sm" type="submit">Deletar</button>
                    </form>  -->
                <form method="POST" action="{{ route('cidades.destroy', $cidade->id_cidade) }}" class="form-inline form-delete">
                    @csrf
                    @method('DELETE')
                    <hidden name="id" value="{{$cidade->id_cidade}}">
                    <button type="submit" class="btn btn-outline-danger btn-sm" name="delete_modal" 
                        data-toggle="tooltip" title='Delete'><i class="fa fa-trash"></i>&nbsp;Deletar</button>
                </form>
            </td>
        </tr>
        @endforeach       
    </tbody>
</table>
<div>
<!-- TEST -->
@endsection

