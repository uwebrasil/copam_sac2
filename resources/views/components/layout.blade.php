

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <meta name="_token" content="{{ csrf_token() }}"/>
      <title>SAC</title>
      <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
      <script src="{{ asset('js/app.js') }}" type="text/js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
   </head>
   <body>
      <div class="navbar navbar-expand-md navbar-light bg-info shadow-sm">
      
         <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
               <div class="imgwrapper">
                  <img src="{{ url('/') }}/images/copam.png" class="img-responsive">
               </div>
               <span style="font-size:smaller;color:white;">SAC - Sistema Agenda de Contatos</span>
            </a>
            @yield('header')
         </div>
      </div >
      <div class="container">
         @yield('content')
      </div>
      <script>  
    /**************************************** AJAX  **************************/
    /*********************************************************************** */           
       jQuery(document).ready(function(){
            
            // post for autocomplete search
            $("#search-box").keyup(function(){
                // e.preventDefault();
                $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')                      
                  }
                });

                $.ajax({
                type: "post",
                url: "{{ route('postClientes') }}",
            
                data: {"_token": "{{ csrf_token() }}","id": $(this).val()},

                beforeSend: function(){                   
                    $("#search-box").css("background","#FFF url(ajax-loader.gif) no-repeat 165px");
                },
                success: function(data){                                 
                    $('#cliente_id_cliente').empty();
                    $('#cliente_id_cliente').append("<option value="+"-1"+">"+"Escolher cliente..."+"</option>");
                    $.each(data, function(index, value) {                                                                
                      $('#cliente_id_cliente').append("<option value="+value.id_cliente+">"+value.nome+"</option>");
                    });                               
                },
                error: function (xhr, ajaxOptions, thrownError) {
                     alert(xhr.status);
                     alert(thrownError);
                     debugger;
                  }   
                }); 
            });
          
        });    
        /**************************************** end AJAX************************/
        /*************************************************************************/          
        // modal confirm eventhandler 
         $('table[data-form="deleteForm"]').on('click', '.form-delete', function(e){               
             e.preventDefault();
             var $form=$(this);  
             $('#confirm').modal({ backdrop: 'static', keyboard: false })
                 .on('click', '#delete-btn', function(){
                     $form.submit();
                 });
         });

         // validate numeric field
         function isNumberKey(evt){
          var charCode = (evt.which) ? evt.which : evt.keyCode
          if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;
          return true;
         }

         // limpar input CPF/CNPJ
         function clearCpfCnpj() {     
          elem = document.getElementById('cpf_cnpj');   
         
          len  = elem.value.replace(/\D/g,'').length;
          if (len==11) {       
           document.getElementById('tipo_pessoa').value = 'F'
          }
          if (len==14) {       
           document.getElementById('tipo_pessoa').value = 'J'
          }
         
          elem.value = elem.value.replace(/\D/g,'');  
         
         return true;
         }
    
    /**************************************** webservice VIACEP **************/
    /*********************************************************************** */    
    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            //document.getElementById('logradouro').value=(conteudo.logradouro);           
            //alert(conteudo.localidade);
        }   //end if.
        else {
            //CEP não Encontrado.                   
            $("#jqueryError").html("<ul><li>CEP não encontrado.</li></ul").fadeIn(2000).fadeOut(5000);         
        }
    }
        
    function pesquisacep(valor) {
       
        //Nova variável "cep" somente com dígitos.
        var cep = valor.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {            
                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.               
                $("#jqueryError").html("");
                $("#jqueryError").html("<ul><li>Formato de CEP inválido..</li></ul").fadeIn(2000).fadeOut(5000);   
            }
        } //end if.       
    };

      </script>
   </body>
</html>

