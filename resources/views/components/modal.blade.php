<div class="modal" id="confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-light">
            <h5 class="modal-title ">Confirmar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>                              
            </div>
            <div class="modal-body">
                <p>Tem certeza, quer deletar?</p>
            </div>
            <div class="modal-footer bg-light">
                <button type="button" class="btn btn-sm btn-danger " id="delete-btn">Deletar</button>
                <button type="button" class="btn btn-sm btn-outline-info" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>