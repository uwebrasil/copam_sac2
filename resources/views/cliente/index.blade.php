

@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
   <span>Clientes&nbsp;listar</span>
</div>
@endsection
@section('content')
<style>
   .uper {
   margin-top: 40px;
   }
</style>
<div class="uper">
@if(session()->get('success'))
<div class="alert alert-success">
   {{ session()->get('success') }}  
</div>
<br />
@endif
@include('components.modal')
{{ $clientes->links() }}
<table class="table table-striped" data-toggle="dataTable" data-form="deleteForm">
   <thead>
      <tr>
         <td>Nome</td>
         <td>Tipo Pessoa</td>
         <td>CPF/CNPJ</td>
         <td>Email</td>
         <td>&nbsp;</td>
         <td><a href="{{ route('clientes.create')}}" class="btn btn-outline-success btn-sm"><i class="fa fa-plus"></i>&nbsp;Adicionar</i></a></td>
      </tr>
   </thead>
   <tbody>
      @foreach($clientes as $cliente)       
      <tr>
         <td>{{$cliente->nome}}</td>
         <td>
            @if($cliente->tipo_pessoa == 'J')
            Pessoa Jurídica
            @endif 
            @if($cliente->tipo_pessoa == 'F')          
            Pessoa Física     
            @endif
         </td>
         <!-- Helper method to format Cpf/Cnpj -->
         <td>{{Helper::formatCnpjCpf($cliente->cpf_cnpj)}}</td>
         <td>{{$cliente->email}}</td>
         <td><a href="{{ route('clientes.edit', $cliente->id_cliente)}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-edit"></i>&nbsp;</i>Editar</a></td>
         <td>
            <!-- <form action="{{ route('clientes.destroy', $cliente->id_cliente)}}" method="post">
               @csrf
               @method('DELETE')
               <button class="btn btn-outline-danger btn-sm" type="submit">Deletar</button>
               </form> -->
            <form method="POST" action="{{ route('clientes.destroy', $cliente->id_cliente) }}" class="form-inline form-delete">
               @csrf
               @method('DELETE')
               <hidden name="id" value="{{$cliente->id_cliente}}">
               <button type="submit" class="btn btn-outline-danger btn-sm" name="delete_modal" 
                  data-toggle="tooltip" title='Delete'><i class="fa fa-trash"></i>&nbsp;Deletar</button>
            </form>
         </td>
      </tr>
      @endforeach       
   </tbody>
</table>
<div>
@endsection

