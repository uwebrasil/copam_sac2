

@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
   <span>Clientes&nbsp;editar</span>
</div>
@endsection
@section('content')
<style>
   .uper {
   margin-top: 40px;
   }
</style>
<div class="card uper">
   <div class="card-header">
      Editar Cliente
   </div>
   <div class="card-body">
      @if ($errors->any())
      <div class="alert alert-danger">
         <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
         </ul>
      </div>
      <br />
      @endif
      <form method="post" action="{{ route('clientes.update', $cliente->id_cliente ) }}" onsubmit="clearCpfCnpj()">
         <div class="form-group">
            @csrf
            @method('PATCH')
            <label for="nome">Nome:</label>
            <input type="text" class="form-control" name="nome" value="{{ $cliente->nome }}"/>
         </div>
         <label for="tipo_pessoa">Tipo Pessoa: o sistema vai informar automaticamente caso achar CPF/CNPJ válido</label>
         <div class="input-group mb-3">          
            <select class="custom-select" id="tipo_pessoa" name="tipo_pessoa">  
            <option @if($cliente->tipo_pessoa == 'X') selected @endif value="X">Escolher...</option>                                 
            <option @if($cliente->tipo_pessoa == 'F') selected @endif value="F">Pessoa Física</option>
            <option @if($cliente->tipo_pessoa == 'J') selected @endif value="J">Pessoa Jurídica</option>
            </select>
         </div>
         <div class="form-group">              
            <label for="cpf_cnpj">CPF/CPNJ: só precisa informar digitos CPF(11) / CNPJ(14)</label>
            <input type="text" class="form-control" id="cpf_cnpj" name="cpf_cnpj"  
               value="{{ Helper::formatCnpjCpf($cliente->cpf_cnpj) }}"
               onkeypress="return isNumberKey(event)"/>             
         </div>
         <div class="form-group">              
            <label for="email">Email:</label>
            <input type="text" class="form-control" name="email" value="{{ $cliente->email }}"/>             
         </div>
         <button type="submit" class="btn btn-primary">Alterar</button>
      </form>
   </div>
</div>
@endsection

