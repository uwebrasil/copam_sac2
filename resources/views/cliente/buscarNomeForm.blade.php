@extends('components.layout')

@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
<span>Clientes&nbsp;buscar</span>
</div>
@endsection

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
  .card-columns {
        column-count: 1;
  } 
  
</style>

<div class="uper">
@isset($clientes)
 @if( $clientes instanceof \Illuminate\Pagination\LengthAwarePaginator )
   {{ $clientes->links() }}
 @endif
@endisset
  <div class="container">

   <div class="card-columns">
                     
      <!-- CLIENTE --> 
      <div class="card-header">Cliente</div>
      <div class="card bg-light">
          <div class="card-body">            
            <form action="{{ route('clientes.buscarNome') }}"   method="post" role="search">    
                @csrf   
                <div class="input-group">
                    <input type="text" class="form-control" name="nome" id="nome"
                        placeholder="buscar nome"> <span class="input-group-btn">
                        <button type="submit" class="btn btn-outline-primary">                     
                        <a href="{{ url('/') }}/clientes/buscarNome"><p class="card-text">Reset</p></a>      
                        </button> 
                        <button type="submit" class="btn btn-outline-primary"> 
                        Buscar nome          
                        </button>
                    </span>
                </div>
            </form>
          </div> 
          </div>
          </div> 

      <div class="card-header">Resultados</div>
       <div class="card bg-light">
          <div class="card-body">
            @isset($clientes)
            <div class="container">
            
            @foreach($clientes as $cliente)
               <div class="row">
                <div class="col-sm-12"><a href="{{ url('/') }}/clientes/{{$cliente->id_cliente}}">{{$cliente->nome}}</a></div>   
               </div>
            @endforeach    
            </div> <!-- container -->                           
            @endisset
       </div>
      </div>              
          </div>
      </div>
  </div> <!-- end container -->
    
<div>
@endsection