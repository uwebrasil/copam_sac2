

@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:30%;">
   <span>Listagem com nome e endereço</span>
</div>
@endsection
@section('content')
<style>
   .uper {
   margin-top: 40px;
   }
</style>
<div class="uper">
@if(session()->get('success'))
<div class="alert alert-success">
   {{ session()->get('success') }}  
</div>
<br />
@endif
{{ $clientes->links() }}
<table class="table table-striped">
   <thead>
      <tr>
         <td>Cliente</td>
         <td>CEP</td>
         <td>Cidade</td>
         <td>Bairro</td>
         <td>Logradouro</td>
         <td>Numero</td>
         <td>Complemento</td>
      </tr>
   </thead>
   <tbody>
      @foreach($clientes as $cliente)       
      <tr>
         <td><a href="{{ url('/') }}/clientes/{{$cliente->id_cliente}}">{{$cliente->clientes_nome}}</a></td>
         <td>{{Helper::formatCEP($cliente->cep)}} </td>
         <td>{{$cliente->cidades_nome}}</td>
         <td>{{$cliente->bairros_nome}}</td>
         <td>{{$cliente->logradouro}}</td>
         <td>{{$cliente->numero}}</td>
         <td>{{$cliente->complemento}}</td>
      </tr>
      @endforeach       
   </tbody>
</table>
<div>
@endsection

