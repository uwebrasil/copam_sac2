

@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
   <span>Clientes&nbsp;cadastrar</span>
</div>
@endsection
@section('content')
<style>
   .uper {
   margin-top: 40px;
   }
</style>
<div class="card uper">
   <div class="card-header">
      SAC - adicionar cliente
   </div>
   <div class="card-body">
      @if ($errors->any())
      <div class="alert alert-danger">
         <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
         </ul>
      </div>
      <br />
      @endif
      <!-- necessary for redirection-->
      @php     
      $url = session()->all()['_previous']['url'] ;   
      $fromNovoCliente = strpos( $url,'clientes') == false ;     
      if($errors->any()) {      
      $isHidden =  isset( session()->all()['_old_input']['hidden'] );
      if ($isHidden) {
      $fromNovoCliente= true;
      }            
      }
      @endphp
      <form method="post" action="{{ route('clientes.store') }}" onsubmit="clearCpfCnpj()">
         <div class="form-group">
            @csrf
            <!-- pass id do novo cliente to endereco.store -->
            @if($fromNovoCliente)
            <input name="hidden" type="hidden" value="dummy"/> 
            @endif              
            <label for="nome">Nome:</label>
            <input type="text" class="form-control" name="nome" 
            @if($errors->any()) value="{{session()->all()['_old_input']['nome'] }}" @endif
            />
         </div>
         <label for="tipo_pessoa">Tipo Pessoa: o sistema vai informar automaticamente caso achar CPF/CNPJ válido</label>
         <div class="input-group mb-3">
            <select class="custom-select" id="tipo_pessoa" name="tipo_pessoa">
               <option selected>Escolher...</option>
               <option
               @if($errors->any() && session()->all()['_old_input']['tipo_pessoa'] == 'F' ) 
               selected
               @endif 
               value="F">Pessoa Física</option>
               <option
               @if($errors->any() && session()->all()['_old_input']['tipo_pessoa'] == 'J' ) 
               selected
               @endif 
               value="J">Pessoa Jurídica</option>
            </select>
         </div>
         <div class="form-group">
            <label for="cpf_cnpj">CPF/CPNJ: só precisa informar digitos CPF(11) / CNPJ(14)</label>
            <!--HTML5 pattern not appropriate here-->
            <input type="text" class="form-control"  id="cpf_cnpj" name="cpf_cnpj"
            @if($errors->any()) value="{{session()->all()['_old_input']['cpf_cnpj'] }}" @endif
            onkeypress="return isNumberKey(event)"/>              
         </div>
         <div class="form-group">              
            <label for="email">Email:</label>
            <input type="text" class="form-control" name="email"
            @if($errors->any()) value="{{session()->all()['_old_input']['email'] }}" @endif
            />              
         </div>
         <button type="submit" class="btn btn-primary">Adicionar</button>
      </form>
   </div>
</div>
@endsection

