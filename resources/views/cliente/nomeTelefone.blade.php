

@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:30%;">
   <span>Listagem com nome e telefone</span>
</div>
@endsection
@section('content')
<style>
   .uper {
   margin-top: 40px;
   }
</style>
<div class="uper">
@if(session()->get('success'))
<div class="alert alert-success">
   {{ session()->get('success') }}  
</div>
<br />
@endif
<form action="{{ route('clientes.buscarNomeTelefone') }}"   method="post" role="search">
   @csrf   
   <div class="input-group">
      <input type="text" class="form-control" name="telefone" id="telefone"
         placeholder="buscar telefones"> 
      <span class="input-group-btn">
         <button type="submit" class="btn btn-outline-primary">
            <a href="{{ url('/') }}/clientes/nomeTelefone">
               <p class="card-text">Reset</p>
            </a>
         </button>
         <button type="submit" class="btn btn-outline-primary"> 
         Buscar telefone              
         </button>
      </span>
   </div>
</form>
<br />
@if( $clientes instanceof \Illuminate\Pagination\LengthAwarePaginator )
{{ $clientes->links() }}
@endif
<table class="table table-striped">
   <thead>
      <tr>
         <td>Nome</td>
         <td>DDD</td>
         <td>Telefone</td>
      </tr>
   </thead>
   <tbody>
      @foreach($clientes as $cliente)       
      <tr>
         <td><a href="{{ url('/') }}/clientes/{{$cliente->id_cliente}}">{{$cliente->nome}}</a></td>
         <td>{{$cliente->ddd}}</td>
         <td>{{$cliente->telefone}}</td>
      </tr>
      @endforeach       
   </tbody>
</table>
<div>
@endsection

