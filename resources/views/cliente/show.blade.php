

@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
   <span>&nbsp;{{$clientes->nome}}</span>
</div>
@endsection
@section('content')
<style>
   .uper {
   margin-top: 40px;
   }
   .card-columns {
   column-count: 1;
   } 
</style>
<div class="uper">
<div class="container">
<div class="card-columns">
   <!-- CLIENTE --> 
   <div class="card-header"><b>Cliente</b></div>
   <div class="card bg-light">
      <div class="card-body">
         <dicv class="container">
         <div class="row">
            <div class="col-sm-3">nome</div>
            <div class="col-sm-9">{{ $clientes->nome}}</div>
         </div>
         <div class="row">
            <div class="col-sm-3">tipo de pessoa</div>
            <div class="col-sm-9">
               @if( $clientes->tipo_pessoa == 'F') pessoa física @endif
               @if( $clientes->tipo_pessoa == 'J') pessoa jurídica @endif
            </div>
         </div>
         <div class="row">
            <div class="col-sm-3">cpf/cnpj</div>
            <div class="col-sm-9">{{ Helper::formatCnpjCpf($clientes->cpf_cnpj) }}</div>
         </div>
         <div class="row">
            <div class="col-sm-3">email</div>
            <div class="col-sm-9">{{ $clientes->email }}</div>
         </div>
      </div>
   </div>
</div>
<!-- CONTATOS (TELEFONES) -->
<div class="card-header"><b>Contatos&nbsp;&nbsp;&nbsp;</b> 
                         <a href="{{ route('contatos.create') }}" class="btn btn-outline-success btn-sm">
                         <i class="fa fa-plus"></i>&nbsp;Adicionar</i></a></div>
<div class="card bg-light">
   <div class="card-body">
      <div class="container">
         @foreach($telefones as $telefone) 
         <div class="row">
            <div class="col-sm-9">{{ Helper::formatTelefone($telefone->ddd.$telefone->telefone)}}</div>
         </div>
         @endforeach    
      </div>
   </div>
   <!-- ENDERECOS -->
   <div class="card-header"><b>Endereços&nbsp;&nbsp;</b><a href="{{ route('enderecos.create') }}" class="btn btn-outline-success btn-sm">
                            <i class="fa fa-plus"></i>&nbsp;Adicionar</a></a></div>
   <div class="card bg-light">
      <div class="card-body">
         <div class="container">
            <div class="row" style="font-weight:bold">
               <div class="col-sm-2">cep</div>
               <div class="col-sm-2">cidade</div>
               <div class="col-sm-2">bairro</div>
               <div class="col-sm-2">logradouro</div>
               <div class="col-sm-2">numero</div>
               <div class="col-sm-2">complemento</div>
            </div>
            @foreach($enderecos as $endereco) 
            <div class="row">
               <div class="col-sm-2">{{ Helper::formatCEP($endereco->cep)}} </div>
               <div class="col-sm-2">{{ $endereco->cnome}} </div>
               <div class="col-sm-2">{{ $endereco->bnome}} </div>
               <div class="col-sm-2">{{ $endereco->logradouro}} </div>
               <div class="col-sm-2">{{ $endereco->numero}} </div>
               <div class="col-sm-2">{{ $endereco->complemento}} </div>
            </div>
            @endforeach    
         </div>
      </div>
   </div>
</div>
<div>
@endsection

