

@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
   <span>Contatos&nbsp;editar</span>
</div>
@endsection
@section('content')
<style>
   .uper {
   margin-top: 40px;
   }
</style>
<div class="card uper">
   <div class="card-header">
      Editar Contato
   </div>
   <div class="card-body">
      @if ($errors->any())
      <div class="alert alert-danger">
         <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
         </ul>
      </div>
      <br />
      @endif
      <form method="post" action="{{ route('contatos.update', $contato->id_contato ) }}">
         <div class="input-group mb-3">
            @csrf
            @method('PATCH')
            <select class="custom-select" id="cliente_id_cliente" name="cliente_id_cliente">
               <option selected>Escolher cliente...</option>
               @foreach ($clientes as $cliente)
               <option  @if($selectedCliente == $cliente->id_cliente) selected @endif
               value="{{ $cliente->id_cliente }}">{{ $cliente->nome }}</option>
               @endforeach
            </select>
         </div>
         <div class="form-group">              
            <label for="ddd">DDD:</label>
            <input type="text" class="form-control" name="ddd" id="ddd"  
               value="{{ $contato->ddd }}"
               maxlength="2" onkeypress="return isNumberKey(event)"/>
         </div>
         <div class="form-group">              
            <label for="telefone">Telefone:</label>
            <input type="text" class="form-control" name="telefone" id="telefone"
               value="{{ $contato->telefone }}"
               maxlength="9" onkeypress="return isNumberKey(event)"/>
         </div>
         <button type="submit" class="btn btn-primary">Alterar</button>
      </form>
   </div>
</div>
@endsection

