

@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
   <span>Contatos&nbsp;listar</span>
</div>
@endsection
@section('content')
<style>
   .uper {
   margin-top: 40px;
   }
</style>
<div class="uper">
@if(session()->get('success'))
<div class="alert alert-success">
   {{ session()->get('success') }}  
</div>
<br />
@endif
@include('components.modal')
{{ $contatos->links() }}
<table class="table table-striped"  data-toggle="dataTable" data-form="deleteForm">
   <thead>
      <tr>
         <td>ID</td>
         <td>Cliente</td>
         <td>DDD</td>
         <td>Telefone</td>
         <td>&nbsp;</td>
         <td><a href="{{ route('contatos.create')}}" class="btn btn-outline-success btn-sm">
            <i class="fa fa-plus"></i>&nbsp;Adicionar</i></a>
         </td>
      </tr>
   </thead>
   <tbody>
      @foreach($contatos as $contato)       
      <tr>
         <td>{{$contato->id_contato}}</td>
         <td>{{$contato->nome}}</td>
         <td>{{$contato->ddd}}</td>
         <td>{{$contato->telefone}}</td>
         <td><a href="{{ route('contatos.edit', $contato->id_contato)}}" class="btn btn-outline-primary btn-sm">
            <i class="fa fa-edit"></i>&nbsp;Editar</i></a>
         </td>
         <td>
            <!-- <form action="{{ route('contatos.destroy', $contato->id_contato)}}" method="post">
               @csrf
               @method('DELETE')
               <button class="btn btn-outline-danger btn-sm" type="submit">Deletar</button>
               </form> -->
            <form method="POST" action="{{ route('contatos.destroy', $contato->id_contato) }}" class="form-inline form-delete">
               @csrf
               @method('DELETE')
               <hidden name="id" value="{{$contato->id_contato}}">
               <button type="submit" class="btn btn-outline-danger btn-sm" name="delete_modal" 
                  data-toggle="tooltip" title='Delete'><i class="fa fa-trash"></i>&nbsp;Deletar</button>
            </form>
         </td>
      </tr>
      @endforeach       
   </tbody>
</table>
<div>
@endsection

