

@extends('components.layout')
@section('header')
<div style="font-size:xx-large;color:white;margin-right:50%;">
   <span>Contatos&nbsp;cadastrar</span>
</div>
@endsection
@section('content')
<style>
   .uper {
   margin-top: 40px;
   }
</style>
<div class="card uper">
   <div class="card-header">
      SAC - adicionar contato
   </div>
   <div class="card-body">
      @if ($errors->any())
      <div class="alert alert-danger">
         <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
         </ul>
      </div>
      <br />
      @endif
      <!-- // determinar de onde o arquivo foi chamado ( Novo Cliente | listar->adicionar ) -->   
      @php
      $url = session()->all()['_previous']['url'] ;
      $fromNovoCliente = strpos( $url,'clientes') != false ; 
      $cliente_id = Str::after($url,'clientes/') ;
      @endphp 
      <form  method="post" action="{{ route('contatos.store') }}">
         <div class="input-group mb-3">
            @csrf
            <!-- passar id do novo cliente ao contato.store -->
            @if($fromNovoCliente)
            <input name="hidden" type="hidden" value="{{ $cliente_id }}"/> 
            @endif
            <select class="custom-select" id="cliente_id_cliente" name="cliente_id_cliente">
               <option selected>Escolher cliente...</option>
               @foreach ($clientes as $cliente)
               <!-- marcar novo cliente -->
               <option 
               @if($fromNovoCliente && ($cliente->id_cliente == $cliente_id)) selected  @endif 
               value="{{ $cliente->id_cliente }}">{{ $cliente->nome }}</option>               
               @endforeach
            </select>
         </div>
         <div class="form-group">              
            <label for="ddd">DDD:</label>
            <input type="text" class="form-control" name="ddd" id="ddd"  maxlength="2" onkeypress="return isNumberKey(event)"/>
         </div>
         <div class="form-group">              
            <label for="telefone">Telefone:</label>
            <input type="text" class="form-control" name="telefone" id="telefone" maxlength="9" onkeypress="return isNumberKey(event)"/>
         </div>
         <button type="submit" class="btn btn-primary">Adicionar</button>
      </form>
   </div>
</div>
@endsection

