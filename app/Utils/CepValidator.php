<?php namespace App\Utils;
/**
 * veja: http://viacep.com.br/ws/$cep/xml/
 * https://zerobugs.com.br/ver-post/como-pegar-endereco-completo-atraves-do-cep-usando-php-45/
*/
class CepValidator
{
    public function validate($attribute, $value, $parameters, $validator)
    {
        return $this->isValidate($attribute, $value);
    }

    protected function isValidate($attribute, $cep)
    {
        $cep = preg_replace("/[^0-9]/", "", $cep);

        if(strlen($cep) != 8) {
            return false;
        }
        
        $url = "http://viacep.com.br/ws/$cep/xml/";
    
        try { 
            $xml = simplexml_load_file($url);
        }
        catch (exception $e) {
            // can't validate, so assume ok
            return true;
        } 

        $erro = strval($xml->erro);
        if ($erro == "true") {
             return false;
        }
        
        return true;

    }
    
}