<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $primaryKey = 'id_cliente';
    
    protected $fillable = array('nome','tipo_pessoa','cpf_cnpj','email');    

    public $timestamps = false;
}
