<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    protected $primaryKey = 'id_contato';
    protected $fillable = [
        'cliente_id_cliente','ddd','telefone'
    ];
    public $timestamps = false;
}
