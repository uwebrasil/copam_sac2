<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UF extends Model
{
   
    public $timestamps = false;
    protected $fillable = array('codigo', 'UF', 'Estado');    

     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UF', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('UF');
            $table->string('Estado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('UF');
    }
}
