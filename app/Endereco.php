<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $primaryKey = 'id_endereco';
    protected $fillable = [
        'bairro_id_bairro',
        'cliente_id_cliente',
        'cep',
        'logradouro',
        'numero',
        'complemento',        
    ];
    public $timestamps = false;
}
