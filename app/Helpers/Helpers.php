<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

class Helper
{   
    // veja: https://gist.github.com/davidalves1/3c98ef866bad4aba3987e7671e404c1e
    public static function formatCnpjCpf($value)
    {
        $cnpj_cpf = preg_replace("/\D/", '', $value);
    
        if (strlen($cnpj_cpf) === 11) {
         return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cnpj_cpf);
        } 
    
     return preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
    }

    // formatar $value (ddd(2 digitos)+numero(8 ou 9 digitos))
    public static function formatTelefone($value) {
      
        $value="(".substr($value,0,2).") ".substr($value,2,-4)." - ".substr($value,-4);      
        return $value;
    }

    // formatar CEP (8 digitos)
    public static function formatCEP($value) {
        if (strlen($value) != 8)
            return $value;            
        return substr($value,0,5).'-'.substr($value,5);                            
    }
}