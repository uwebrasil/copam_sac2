<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use \App\Cliente;
use Response;

class AjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function view()
    {
        return view('test');
    }
    
    /**
     * Create a new controller instance.
     *
     * @return clientes
     */

    public function getClientes()
    {       
        $clientes = DB::table('clientes')
        ->select('id_cliente','nome')                
        ->orderBy('nome')
        ->get();
        return response()->json($clientes);
    }
    /**
     * Create a new controller instance.
     *
     * @return clientes
     */

    public function postClientes(Request $request)
    {            
        $all= $request->all();  
        $search =  $all['id'];
       
        $raw  = "";
        $raw .= "SELECT id_cliente, nome FROM clientes ";        
        $raw .= "Where nome LIKE '%".$search."%' ";
        $raw .= " ORDER BY nome LIMIT 100";
       
        $clientes = DB::select($raw);
        return response()->json($clientes);
    }   
}
