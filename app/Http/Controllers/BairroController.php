<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BairroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       
        $bairros = DB::table('bairros')
                   ->join('cidades','bairros.cidade_id_cidade','=','cidades.id_cidade') 
                   ->select('bairros.id_bairro','bairros.nome','bairros.cidade_id_cidade','cidades.nome as cnome')
                   ->orderBy('bairros.nome')                                  
                   ->paginate(10);                         
        return view('bairro.index', ['bairros' => $bairros]);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {             
       $cidades = DB::table('cidades')
                         ->select('nome','id_cidade')
                         ->orderBy('nome', 'asc')
                         ->get();       
       return view('bairro.create')->with(array('cidades'=>$cidades));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {               
      
        $validatedData = $request->validate([
            'nome' => 'nullable',   
            'cidade_id_cidade' => 'required|numeric'            
        ]       
        ,['Favor, escolher cidade!']     
        );
        $show = \App\Bairro::create($validatedData);   
        
        return redirect('/bairros')->with('success', 'Bairro foi gravada');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {                           
        $cidades = DB::table('cidades')
                         ->select('nome','id_cidade')
                         ->orderBy('nome', 'asc')
                         ->get();     
        $selectedCidade = DB::table('bairros')
                         ->select('cidade_id_cidade')                          
                         ->where('id_bairro',$id)
                         ->first()->cidade_id_cidade;                                  
        $bairro = \App\Bairro::where('id_bairro', $id)->first();       
        return view('bairro.edit', compact('bairro'))
                    ->with(array('cidades'=>$cidades))          
                    ->with(array('selectedCidade'=>$selectedCidade)); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {      
        $validatedData = $request->validate([
            'nome' => 'nullable',   
            'cidade_id_cidade' => 'required|numeric'           
        ]       
        ,['Favor, escolher cidade!']       
        );
        $nome = $validatedData["nome"];
        $cidade_id_cidade = $validatedData["cidade_id_cidade"];  
       
        DB::table('bairros')
            ->where('id_bairro', $id)
            ->update(['nome' => $nome,'cidade_id_cidade' => $cidade_id_cidade]);
       
        // \App\Cidade::whereId($id)->update($validatedData);        
        // does not work here cause Laravel assumes a field with name "id" as primaryKey
        // setting custom primaryKey in Model also does not work ...                       

        return redirect('/bairros')->with('success', 'Bairro foi alterada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {                
        DB::table('bairros')
            ->where('id_bairro', $id)
            ->delete();       
        return redirect('/bairros')->with('success', 'Bairro foi deletada');
    }
}
