<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        $clientes = DB::table('clientes')
                   ->orderBy('nome')
                   ->paginate(10);      
        return view('cliente.index', ['clientes' => $clientes]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {             
       return view('cliente.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        // sanitize 
        if ($request->tipo_pessoa == 'Escolher...') {
            $request->merge(array('tipo_pessoa' => 'X'));
        }
        // determines location of redirect
        $hidden = $request->request->get('hidden'); 
        
        $validatedData = $request->validate([
            'nome' => 'nullable',
            'tipo_pessoa' => 'nullable|max:1',     
            'cpf_cnpj' => 'nullable|cpf|unique:clientes',
            'email' => 'nullable|email'      
        ]
        ,['cpf_cnpj' =>' O CPF/CNPF já está sendo utilizado.']     
        );
        $show = \App\Cliente::create($validatedData);   
        
         // voltar para show
         if (!is_null($hidden)) {
            return redirect('/clientes/'.$show->id_cliente);
        }
        // voltar para clientes.index
        return redirect('/clientes')->with('success', 'Cliente foi gravado');              
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {                                  
        $cliente = \App\Cliente::where('id_cliente', $id)->first();       
        return view('cliente.edit', compact('cliente'));           
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {      
        $validatedData = $request->validate([
            'nome' => 'nullable',
            'tipo_pessoa' => 'required|max:1',     
            'cpf_cnpj' => 'nullable|cpf',
            'email' => 'nullable|email'      
        ]   
        ,['cpf_cnpj' =>' O CPF/CNPF já está sendo utilizado.']              
        ); 
        $nome = $validatedData["nome"];
        $tipo_pessoa = $validatedData["tipo_pessoa"];
        $cpf_cnpj = $validatedData["cpf_cnpj"];
        $email = $validatedData["email"];
       
       
        DB::table('clientes')
            ->where('id_cliente', $id)
            ->update([
                'nome' => $nome,
                'tipo_pessoa' => $tipo_pessoa,
                'cpf_cnpj' => $cpf_cnpj,
                'email' => $email,
                ]);
       
        // \App\Cidade::whereId($id)->update($validatedData);        
        // does not work here cause Laravel assumes a field with name "id" as primaryKey
        // setting custom primaryKey in Model also does not work ...                       

        return redirect('/clientes')->with('success', 'Cliente foi alterada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {                
        DB::table('clientes')
            ->where('id_cliente', $id)
            ->delete();       
        return redirect('/clientes')->with('success', 'Cliente foi deletada');
    }

    /**
     * Listagem com nome e telefone
     */
    public function listNomeTelefone()
    {
        $clientes = DB::table('clientes')
                   ->join('contatos', 'contatos.cliente_id_cliente', '=', 'clientes.id_cliente')  
                   ->select('nome','ddd','telefone','id_cliente')
                   ->orderBy('nome')
                   ->orderBy('ddd')
                   ->orderBy('telefone')
                   ->paginate(10);             
        return view('cliente.nomeTelefone', ['clientes' => $clientes]);
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id)
     {        
        $clientes = DB::table('clientes')                    
                    ->select('id_cliente', 'nome', 'tipo_pessoa', 'cpf_cnpj', 'email')
                    ->where('id_cliente',$id)->first();
        $telefones = DB::table('contatos')                    
                    ->select('id_contato', 'cliente_id_cliente', 'ddd', 'telefone')
                    ->where('cliente_id_cliente',$id)->get();   
        $enderecos  = DB::table('enderecos')  
                     ->join('bairros','enderecos.bairro_id_bairro','=','bairros.id_bairro')
                     ->join('cidades', 'bairros.cidade_id_cidade', '=', 'cidades.id_cidade')                  
                    ->select('id_endereco', 'bairro_id_bairro', 'cliente_id_cliente', 'cep', 'logradouro','numero','complemento',
                             'id_bairro','cidade_id_cidade', 'bairros.nome as bnome',
                             'id_cidade','cidades.nome as cnome','UF')
                    ->where('cliente_id_cliente',$id)->get();                       

        return view('cliente.show', ['clientes' => $clientes, 'telefones' => $telefones, 'enderecos'=> $enderecos]);
     }
    /**
     * Listagem com nome e telefone
     */
    public function buscarNomeTelefone(Request $request)
    {
        $telefone = $request->input('telefone');

        $raw  = "";
        $raw .= "SELECT id_cliente, id_contato, nome, ddd, telefone FROM clientes cl, contatos co ";
        $raw .= "WHERE cl.id_cliente = co.cliente_id_cliente ";
        $raw .= "AND telefone LIKE '%".$telefone."%' ";
        $raw .= "ORDER BY nome, ddd, telefone";

        // raw query faster, but without pagination
        $clientes = DB::select($raw);
                         
        return view('cliente.nomeTelefone', ['clientes' => $clientes]);
    }

    /**
     * Listagem com nome e endereco
     */
    public function listNomeEndereco()
    {
        $clientes = DB::table('clientes')
                   ->join('enderecos', 'enderecos.cliente_id_cliente', '=', 'clientes.id_cliente')  
                   ->join('bairros', 'enderecos.bairro_id_bairro', '=', 'bairros.id_bairro')
                   ->join('cidades', 'bairros.cidade_id_cidade', '=', 'cidades.id_cidade')
                   ->select('clientes.nome as clientes_nome','cep',
                            'cidades.nome as cidades_nome',
                            'bairros.nome as bairros_nome',
                            'logradouro','numero','complemento',
                            'clientes.id_cliente'
                            )
                   ->orderBy('clientes.nome')
                   ->orderBy('cidades.nome')
                   ->orderBy('bairros.nome')

                   ->paginate(10);             
        return view('cliente.nomeEndereco', ['clientes' => $clientes]);
    }

    /**
     * Busca por nome formulario
     */
    public function buscarNomeForm() {
       
        return view('cliente.buscarNomeForm');
    }
    /**
     * Busca por nome
     */
    public function buscarNome(Request $request) {
       
        $start = microtime(true);
        $nome = $request->nome;

        $clientes = DB::table('clientes')
                    ->select('id_cliente','nome')
                    ->where('nome','like','%'.$nome.'%')
                    ->orderBy('nome')                   
                    ->paginate(20);
                    //->paginate(20)->dump();
       
        return view('cliente.buscarNomeForm', ['clientes' => $clientes]);
    }
    /**
     * Teste webservice VIACEP
     */
    public function checkCEP() {
        $cep = '44190000';
        $cep = '44190001';
        $cep = preg_replace("/[^0-9]/", "", $cep);
        $url = "http://viacep.com.br/ws/$cep/xml/";

        $xml = simplexml_load_file($url);
        var_dump($xml);
        print_r($xml->localidade);
        print_r(gettype($xml->localidade));  
        print_r($xml->uf == "BA");

        $ba = strval($xml->uf);
        print_r(gettype($ba)); 

        $ddd = strval($xml->ddd);
        $localidade = strval($xml->localidade);
        $uf = strval($xml->uf);
        $logradouro = strval($xml->logradouro);
        $complemento = strval($xml->complemento);

        $bairro = strval($xml->bairro);

        print_r( gettype($bairro)) ;
        print_r(strlen($bairro));

        $erro = strval($xml->erro);
        if ($erro == "true") print_r("TRUE");

        dd($xml);
        return view('home');
    }
}
