<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {                                          
        $contatos = DB::table('contatos')
                   ->join('clientes','contatos.cliente_id_cliente','=','clientes.id_cliente') 
                   ->select('contatos.id_contato','contatos.ddd','contatos.telefone','contatos.cliente_id_cliente','clientes.nome')
                   ->orderBy('clientes.nome')                                  
                   ->paginate(10);                         
        return view('contato.index', ['contatos' => $contatos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {          
       //$latest = DB::table('clientes')->orderBy('id_cliente', 'DESC')->first();
       $clientes = DB::table('clientes')
                         ->select('nome','id_cliente')
                         ->orderBy('nome', 'asc')
                         ->get();       
       return view('contato.create')->with(array('clientes'=>$clientes));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {                         
        // get hidden id_cliente if defined
        $hidden = $request->request->get('hidden');       
        $validatedData = $request->validate([              
            'cliente_id_cliente' => 'required|numeric',
            'ddd' => 'nullable',
            'telefone' => 'nullable'            
        ]
        ,['Favor, escolher cliente!']       
        );
  
        $show = \App\Contato::create($validatedData);   
       
        // voltar para show
        if (!is_null($hidden)) {
            return redirect('/clientes/'.$hidden);
        }
        // voltar para contato.index
        return redirect('/contatos')->with('success', 'Contato foi gravada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {                           
        $clientes = DB::table('clientes')
                         ->select('nome','id_cliente')
                         ->orderBy('nome', 'asc')
                         ->get();      
        $selectedCliente = DB::table('contatos')
                         ->select('cliente_id_cliente')                          
                         ->where('id_contato',$id)
                         ->first()->cliente_id_cliente;
                         
        $contato = \App\Contato::where('id_contato', $id)->first();       
        return view('contato.edit', compact('contato'))
                     ->with(array('clientes'=>$clientes))
                     ->with(array('selectedCliente'=>$selectedCliente));           
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {      
        $validatedData = $request->validate([
            'cliente_id_cliente' => 'required|numeric',
            'ddd' => 'nullable',
            'telefone' => 'nullable'              
        ]
        ,['Favor, escolher cliente!']       
        );
        $ddd = $validatedData["ddd"];
        $telefone = $validatedData["telefone"];
        $cliente_id_cliente = $validatedData["cliente_id_cliente"];  
      
        DB::table('contatos')
            ->where('id_contato', $id)
            ->update(['cliente_id_cliente' => $cliente_id_cliente,'ddd' => $ddd,'telefone'=>$telefone]);
                        
        return redirect('/contatos')->with('success', 'Contato foi alterada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {                
        DB::table('contatos')
            ->where('id_contato', $id)
            ->delete();       
        return redirect('/contatos')->with('success', 'Contato foi deletada');
    }
}
