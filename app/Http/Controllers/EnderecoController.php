<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EnderecoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {                                          
        $enderecos = DB::table('enderecos')
                   ->join('clientes','enderecos.cliente_id_cliente','=','clientes.id_cliente') 
                   ->join('bairros','enderecos.bairro_id_bairro','=','bairros.id_bairro')
                   ->join('cidades', 'bairros.cidade_id_cidade', '=', 'cidades.id_cidade')
                   ->select('id_endereco','clientes.nome as cnome','bairros.nome as bnome',
                            'cep','logradouro','numero','complemento',
                            'cidades.nome as city')
                   ->orderBy('cep')                                  
                   ->paginate(10);                         
        return view('endereco.index', ['enderecos' => $enderecos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {             
       $clientes = DB::table('clientes')
                         ->select('nome','id_cliente')
                         ->orderBy('nome', 'asc')
                         ->get();    
       
       $bairros  = DB::table('bairros')
                        ->join('cidades', 'bairros.cidade_id_cidade', '=', 'cidades.id_cidade')  
                        ->select( DB::raw("CONCAT(bairros.nome,' (',cidades.nome,')') as nome"),'id_bairro' )
                        ->orderBy('bairros.nome', 'asc')
                        ->orderBy('cidades.nome', 'asc')
                        ->get();       

       return view('endereco.create')
              ->with(array('clientes'=>$clientes))
              ->with(array('bairros'=>$bairros));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {                         
        // get hidden id_cliente if defined
        $hidden = $request->request->get('hidden'); 
        $validatedData = $request->validate([      
            'bairro_id_bairro' => 'required|numeric',        
            'cliente_id_cliente' => 'required|numeric',
            'cep' => 'nullable|cep',
            'logradouro' => 'nullable',
            'numero' => 'nullable|numeric',
            'complemento' => 'nullable',
        ],
        ['Favor, escolher bairro e cidade!']       
        );
  
        $show = \App\Endereco::create($validatedData);   
              
        // voltar para show
        if (!is_null($hidden)) {
            return redirect('/clientes/'.$hidden);
        }
        // voltar para contato.index
        return redirect('/enderecos')->with('success', 'Endereco foi gravado');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {                           
        $clientes = DB::table('clientes')
                         ->select('nome','id_cliente')
                         ->orderBy('nome', 'asc')
                         ->get();      

        $selectedCliente = DB::table('enderecos')
                         ->select('cliente_id_cliente')                          
                         ->where('id_endereco',$id)
                         ->first()->cliente_id_cliente;
       
        $bairros         = DB::table('bairros')
                         ->join('cidades', 'bairros.cidade_id_cidade', '=', 'cidades.id_cidade')  
                         ->select( DB::raw("CONCAT(bairros.nome,' (',cidades.nome,')') as nome"),'id_bairro' )
                         ->orderBy('bairros.nome', 'asc')
                         ->orderBy('cidades.nome', 'asc')
                         ->get();       

        $selectedBairro = DB::table('enderecos')
                         ->select('bairro_id_bairro')                          
                         ->where('id_endereco',$id)
                         ->first()->bairro_id_bairro;                                        
                         
        $endereco = \App\Endereco::where('id_endereco', $id)->first();       
        return view('endereco.edit', compact('endereco'))
                     ->with(array('clientes'=>$clientes))
                     ->with(array('selectedCliente'=>$selectedCliente))     
                     ->with(array('bairros'=>$bairros))
                     ->with(array('selectedBairro'=>$selectedBairro));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {    
        
        $validatedData = $request->validate([
            'bairro_id_bairro' => 'required|numeric',
            'cliente_id_cliente' => 'required|numeric',
            'cep' => 'nullable|cep',
            'logradouro' => 'nullable',
            'numero' => 'nullable|numeric',              
            'complemento' => 'nullable'
        ],
        ['Favor, escolher bairro e cidade!']     
        );
        $cep = $validatedData["cep"];
        $logradouro = $validatedData["logradouro"];
        $cliente_id_cliente = $validatedData["cliente_id_cliente"];
        $bairro_id_bairro = $validatedData["bairro_id_bairro"]; 
        $numero = $validatedData["numero"];
        $complemento = $validatedData["complemento"];  
      
        DB::table('enderecos')
            ->where('id_endereco', $id)
            ->update(['cliente_id_cliente' => $cliente_id_cliente,
                      'bairro_id_bairro' => $bairro_id_bairro,
                      'cep' => $cep,
                      'numero' => $numero,
                      'complemento' => $complemento,
                      'logradouro'=>$logradouro]);
                        
        return redirect('/enderecos')->with('success', 'Endereço foi alterada');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {                
        DB::table('enderecos')
            ->where('id_endereco', $id)
            ->delete();       
        return redirect('/enderecos')->with('success', 'Endereço foi deletado');
    }
}
