<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CidadeController extends Controller
{    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        $cidades = DB::table('cidades')
                   ->orderBy('nome')
                   ->paginate(10);      
        return view('cidade.index', ['cidades' => $cidades]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // get all possible UFs from table
       // and pass to view      
       $ufs = DB::table('u_f_s')
                         ->select('UF','Estado')
                         ->orderBy('UF', 'asc')
                         ->get();       
       return view('cidade.create')->with(array('ufs'=>$ufs));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
        // sanitize 
        if ($request->UF == 'Escolher...') {
            $request->merge(array('UF' => ''));
        }
        $validatedData = $request->validate([
            'nome' => 'nullable',
            'UF' => 'nullable|max:2',           
        ]);
        $show = \App\Cidade::create($validatedData);   
        return redirect('/cidades')->with('success', 'Cidade foi gravada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {                           
        $ufs = DB::table('u_f_s')
                         ->select('UF','Estado')
                         ->orderBy('UF', 'asc')
                         ->get();   
        $selectedUF = DB::table('cidades')
                         ->select('UF')                          
                         ->where('id_cidade',$id)
                         ->first()->UF;                         
        $cidade = \App\Cidade::where('id_cidade', $id)->first();       
        return view('cidade.edit', compact('cidade'))
                    ->with(array('ufs'=>$ufs))
                    ->with(array('selectedUF'=>$selectedUF));           
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {      
        $validatedData = $request->validate([
            'nome' => 'nullable',
            'UF' => 'nullable|max:2',           
        ]
        );      
        $nome = $validatedData["nome"];
        $UF = $validatedData["UF"];  
       
        DB::table('cidades')
            ->where('id_cidade', $id)
            ->update(['nome' => $nome,'UF' => $UF]);
       
        // \App\Cidade::whereId($id)->update($validatedData);        
        // does not work here cause Laravel assumes a field with name "id" as primaryKey
        // setting custom primaryKey in Model also does not work ...                       

        return redirect('/cidades')->with('success', 'Cidade foi alterada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {                
        DB::table('cidades')
            ->where('id_cidade', $id)
            ->delete();       
        return redirect('/cidades')->with('success', 'Cidade foi deletada');
    }
}
