<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bairro extends Model
{
    protected $primaryKey = 'id_bairro';
    protected $fillable = [
        'cidade_id_cidade','nome'
    ];
    public $timestamps = false;
}
