<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);

        Validator::extend('cpf', '\App\Utils\CpfValidator@validate',
                          ':attribute inválido:  Gerador de CPF/CNPJ: https://www.4devs.com.br/gerador_de_cnpj');
        Validator::extend('cep', '\App\Utils\CepValidator@validate',
                          ':attribute formato inválido ou :attribute não encontrado.');                          
    }
}
