<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');


Route::get('/ajax', 'AjaxController@view');
Route::post('/ajax', 'AjaxController@send_http_request')->name('ajaxRequest.post');

Route::get('/getClientes', 'AjaxController@getClientes')->name('getClientes');
Route::post('/postClientes', 'AjaxController@postClientes')->name('postClientes');



Route::get('clientes/nomeTelefone', 'ClienteController@listNomeTelefone')->middleware('auth');
Route::get('clientes/nomeEndereco', 'ClienteController@listNomeEndereco')->middleware('auth');

Route::get('clientes/checkCEP', 'ClienteController@checkCEP')->middleware('auth');

Route::get('clientes/buscarNomeForm', 'ClienteController@buscarNomeForm')->middleware('auth');
Route::get('clientes/buscarNome', 'ClienteController@buscarNome')->middleware('auth');

Route::post('clientes/buscarNomeTelefone', 'ClienteController@buscarNomeTelefone')
            ->name('clientes.buscarNomeTelefone')->middleware('auth');
Route::post('clientes/buscarNome', 'ClienteController@buscarNome')
            ->name('clientes.buscarNome')->middleware('auth');          
            
            

Route::resource('cidades','CidadeController')->middleware('auth');

Route::resource('bairros','BairroController')->middleware('auth');

Route::resource('clientes','ClienteController')->middleware('auth');

Route::resource('contatos','ContatoController')->middleware('auth');

Route::resource('enderecos','EnderecoController')->middleware('auth');

/* quick and dirty
// dump sql query
// veja:https://stackoverflow.com/questions/27753868/how-to-get-the-query-executed-in-laravel-5-dbgetquerylog-returning-empty-ar
\Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
    echo'<pre>';
    var_dump($query->sql);
    var_dump($query->bindings);
    var_dump($query->time);
    echo'</pre>';
});
*/